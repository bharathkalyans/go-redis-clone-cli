package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

type Database struct {
	arrays map[string][]int
	mutex  sync.Mutex
}

func NewDatabase() *Database {
	return &Database{
		arrays: make(map[string][]int),
	}
}

func (db *Database) createArray(name string, data ...int) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	if _, exists := db.arrays[name]; exists {
		return fmt.Errorf("array '%s' already exists", name)
	}

	db.arrays[name] = data
	return nil
}

func (db *Database) showArray(name string) ([]int, error) {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	array, exists := db.arrays[name]
	if !exists {
		return nil, fmt.Errorf("array '%s' does not exist", name)
	}

	return array, nil
}

func (db *Database) deleteArray(name string) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	_, exists := db.arrays[name]
	if !exists {
		return fmt.Errorf("array '%s' does not exist", name)
	}

	delete(db.arrays, name)
	return nil
}

func (db *Database) mergeArrays(destName, srcName string) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	dest, destExists := db.arrays[destName]
	if !destExists {
		return fmt.Errorf("array '%s' does not exist", destName)
	}

	src, srcExists := db.arrays[srcName]
	if !srcExists {
		return fmt.Errorf("array '%s' does not exist", srcName)
	}

	db.arrays[destName] = append(dest, src...)
	return nil
}

func (db *Database) SaveToFile(filename string) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	lockFile, err := os.Create(filename + ".lock")
	if err != nil {
		return err
	}
	defer os.Remove(filename + ".lock")
	defer lockFile.Close()

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	if err := encoder.Encode(db.arrays); err != nil {
		return err
	}

	return nil
}

func (db *Database) LoadFromFile(filename string) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	lockFile, err := os.Create(filename + ".lock")
	if err != nil {
		return err
	}
	defer os.Remove(filename + ".lock")
	defer lockFile.Close()

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&db.arrays); err != nil {
		return err
	}

	return nil
}

func (db *Database) ReloadFromFile(filename string) error {
	db.mutex.Lock()
	defer db.mutex.Unlock()

	// Clear the existing data
	db.arrays = make(map[string][]int)

	// Load data from file
	lockFile, err := os.Create(filename + ".lock")
	if err != nil {
		return err
	}
	defer os.Remove(filename + ".lock")
	defer lockFile.Close()

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(&db.arrays); err != nil {
		return err
	}

	return nil
}

func main() {
	db := NewDatabase()

	if len(os.Args) > 1 && os.Args[1] == "new" {
		if _, err := os.Stat(".wkn"); err == nil {
			fmt.Println("Db file already present")
			return
		}
	}

	if len(os.Args) > 2 && os.Args[1] == "--db-path" {
		if err := db.LoadFromFile(os.Args[2]); err != nil {
			fmt.Println("Error:", err)
			return
		}
	} else {
		if _, err := os.Stat(".wkn"); err == nil {
			if err := db.LoadFromFile(".wkn"); err != nil {
				fmt.Println("Error:", err)
				return
			}
		}
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("wkn> ")

	for {
		input, _ := reader.ReadString('\n')
		input = strings.TrimSpace(input)

		if input == "exit" {
			if err := db.SaveToFile(".wkn"); err != nil {
				fmt.Println("Error:", err)
			}
			fmt.Println("Bye!")
			return
		}

		db.ReloadFromFile(".wkn")

		args := strings.Split(input, " ")
		if len(args) == 0 {
			fmt.Println("Unknown command")
			continue
		}

		command := args[0]
		switch command {
		case "":
		case "help":
			PrintHelpInstructions()
		case "new":
			if len(args) < 2 {
				fmt.Println("Error: Not enough arguments. Usage: new <array_name> [data...]")
				fmt.Print("wkn> ")
				continue
			}
			name := args[1]
			data := []int{}
			if len(args) > 2 {
				for _, d := range args[2:] {
					num, err := strconv.Atoi(d)
					if err != nil {
						fmt.Println("Error:", err)
						continue
					}
					data = append(data, num)
				}
			}
			if err := db.createArray(name, data...); err != nil {
				fmt.Println("Error:", err)
			} else {
				fmt.Printf("CREATED (%d)\n", len(data))
				if err := db.SaveToFile(".wkn"); err != nil {
					fmt.Println("Error:", err)
				}
			}
		case "show":
			if len(args) < 2 {
				fmt.Println("Error: Not enough arguments. Usage: show <array_name>")
				fmt.Print("wkn> ")
				continue
			}
			name := args[1]
			array, err := db.showArray(name)
			if err != nil {
				fmt.Println("Error:", err)
			} else {
				fmt.Println(array)
			}
		case "del":
			if len(args) < 2 {
				fmt.Println("Error: Not enough arguments. Usage: del <array_name>")
				fmt.Print("wkn> ")
				continue
			}
			name := args[1]
			if err := db.deleteArray(name); err != nil {
				fmt.Println("Error:", err)
			} else {
				fmt.Println("DELETED")
				if err := db.SaveToFile(".wkn"); err != nil {
					fmt.Println("Error:", err)
				}
			}
		case "merge":
			if len(args) < 3 {
				fmt.Println("Error: Not enough arguments. Usage: merge <dest_array_name> <src_array_name>")
				fmt.Print("wkn> ")
				continue
			}
			destName := args[1]
			srcName := args[2]
			if err := db.mergeArrays(destName, srcName); err != nil {
				fmt.Println("Error:", err)
			} else {
				fmt.Println("MERGED")
				if err := db.SaveToFile(".wkn"); err != nil {
					fmt.Println("Error:", err)
				}
			}
		default:
			fmt.Printf("Error: '%s' is not a supported operation\n", command)
		}

		fmt.Print("wkn> ")
	}
}

func PrintHelpInstructions() {
	fmt.Println("Available commands:")
	fmt.Println("  new            - Create a new array using :: new <array_name> [data . . .]")
	fmt.Println("  del            - Delete an array :: del <array_name>")
	fmt.Println("  show           - Show contents of Array :: show <array_name>")
	fmt.Println("  merge          - Merge contents of Arrays :: merge <array_name1> <array_name2>")
	fmt.Println("  help           - Show available commands")
	fmt.Println("  exit           - Exit the REPL")
}
