### Golang Assignment 2



##### Commands
* ./wkn new - To create a new DB
* ./wkn --db-path filePath - To give filepath of the db file
* new <array_name> - To create a new array
* new <array_name> . . . - Create a new array and add data
* del <array_name> - To delete an array
* merge <array_name1> <array_name2> - To merge 2 existing arrays
* help - Displays set of Instructions and Commands that can be used.
